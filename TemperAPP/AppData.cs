﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TemperAPP
{
    //Singleton
    public class AppData
    {
        private static AppData instance;

        public double Temperature { get; set; }
        public double UserLatitude { get; set; }
        public double UserLongitude { get; set; }
        public double HomeLatitude { get; set; }
        public double HomeLongitude { get; set; }
        public double DistanceFromHome { get; set; }
        public bool HeatingWorking { get; set; }
        public bool HeatingOnForced { get; set; }

        private AppData()
        {
            Temperature = 20.5;
            UserLatitude = 49.792000;
            UserLongitude = 19.059108;
            HomeLatitude = 49.799591;
            HomeLongitude = 19.013139;
            DistanceFromHome = 5000;
            HeatingWorking = false;
            HeatingOnForced = false;
        }

        public static AppData Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new AppData();
                }
                return instance;
            }
        }
    }
}
