﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Cors;

namespace TemperAPP.Controllers
{
    [Route("/[controller]")]
    public class ApiController : Controller
    {
        private AppData _data;
        public ApiController()
        {
            _data = AppData.Instance;
        }
        //halo
        [HttpGet("help")]
        [DisableCors]
        public string GetHelp()
        {
            return "Api/all - pobierz wszystkie informacje\n" +
                "Api/temp - pobierz temperature\n" +
                "Api/loc - pobierz lokalizacje\n" +
                "Api/heat - sprawdz czy dziala ogrzewanie\n" +
                "Api/heat01 - sprawdz czy dziala ogrzewanie(0 nie, 1 tak)\n" +
                "Api/heat/on - forcuj wlączenie ogrzewania\n" +
                "Api/heat/off - przestań forcować ogrzewanie\n" +
                "Api/dist - pobierz dystans od domu w metrach\n" +
                "Api/temp/{temp} - ustaw temperature\n" +
                "Api/loc/{lat}/{lon} - ustaw lokalizacje wlasną\n" +
                "Api/hloc/{lat}/{lon} - ustaw lokalizacje domu\n";
        }

        [HttpGet("all")]
        [DisableCors]
        public JsonResult GetAll()
        {
            return Json(new {
                temperature = _data.Temperature,
                longitude = _data.UserLongitude,
                latitude = _data.UserLatitude,
                homeLongitude = _data.HomeLongitude,
                homeLatitude = _data.HomeLatitude,
                heating = _data.HeatingWorking,
                heating01 = _data.HeatingWorking?1:0,
                distance = _data.DistanceFromHome,
                heatingForced = _data.HeatingOnForced});
        }

        [HttpGet("temp")]
        [DisableCors]
        public JsonResult GetTemp()
        {
            return Json(new { temperature = _data.Temperature });
        }

        [HttpGet("loc")]
        [DisableCors]
        public JsonResult GetLoc()
        {
            return Json(new { longitude = _data.UserLongitude, latitude = _data.UserLatitude });
        }

        [HttpGet("heat")]
        [DisableCors]
        public JsonResult GetHeat()
        {
            return Json(new { heat = _data.HeatingWorking});
        }

        [HttpGet("heat01")]
        [DisableCors]
        public int GetHeat01()
        {
            return _data.HeatingWorking ? 1: 0;
        }

        [HttpGet("heat/on")]
        [DisableCors]
        public JsonResult GetHeatOn()
        {
            _data.HeatingOnForced = true;
            _data.HeatingWorking = true;
            return Json(new { message = "Ogrzewanie włączone" });
        }

        [HttpGet("heat/off")]
        [DisableCors]
        public JsonResult GetHeatOff()
        {
            _data.HeatingOnForced = false;
            if (_data.DistanceFromHome > 2000)
            {
                _data.HeatingWorking = false;
            }
            else
            {
                _data.HeatingWorking = true;
            }
            return Json(new { message = "Ogrzewanie wyłączone" });
        }

        [HttpGet("dist")]
        [DisableCors]
        public JsonResult GetDistance()
        {
            return Json(new { distance = _data.DistanceFromHome });
        }

        // GET /Api/temp/5
        [HttpGet("temp/{temperature}")]
        [DisableCors]
        public JsonResult Get(double temperature)
        {
            _data.Temperature = temperature;
            return Json(new { message = "Ustawiono temperature na: " + temperature.ToString() });
        }

        //GET /loc/5.53534/6.3443
        [HttpGet("loc/{lat}/{lon}")]
        [DisableCors]
        public JsonResult Get(double lat, double lon)
        {
            _data.UserLatitude = lat;
            _data.UserLongitude = lon;
            _data.DistanceFromHome = CalculateDistance(lon, lat, _data.HomeLongitude, _data.HomeLatitude);
            if(_data.DistanceFromHome > 2000 && !_data.HeatingOnForced)
            {
                _data.HeatingWorking = false;
            }
            else
            {
                _data.HeatingWorking = true;
            }
            return Json(new { message = "Ustawiono pozycje na: " + lat.ToString() + "  " + lon.ToString() });
        }

        //GET /hloc/5.53534/6.3443
        [HttpGet("hloc/{lat}/{lon}")]
        [DisableCors]
        public JsonResult GetHloc(double lat, double lon)
        {
            _data.HomeLatitude = lat;
            _data.HomeLongitude = lon;
            _data.DistanceFromHome = CalculateDistance(lon, lat, _data.HomeLongitude, _data.HomeLatitude);
            if (_data.DistanceFromHome > 2000 && !_data.HeatingOnForced)
            {
                _data.HeatingWorking = false;
            }
            else
            {
                _data.HeatingWorking = true;
            }
            return Json(new { message = "Ustawiono pozycje domu na: " + lat.ToString() + "  " + lon.ToString() });
        }

        private double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        private double CalculateDistance(double lon1, double lat1, double lon2, double lat2)
        {
            double R = 6371;
            double dLat = DegreeToRadian(lat2 - lat1);
            double dLon = DegreeToRadian(lon2 - lon1);
            double a = Math.Sin(dLat / 2.0) * Math.Sin(dLat / 2.0) +
                    Math.Cos(DegreeToRadian(lat1)) * Math.Cos(DegreeToRadian(lat2)) *
                    Math.Sin(dLon / 2.0) * Math.Sin(dLon / 2.0);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            double d = R * c;
            return d*1000.0;
        }
    }
}
